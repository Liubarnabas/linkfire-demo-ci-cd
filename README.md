# linkfire demo for ci cd pipeline with Hello Application example

Following the guide link: [deploy-to-aws-eks-kubernetes](https://support.atlassian.com/bitbucket-cloud/docs/deploy-to-aws-eks-kubernetes)

This example shows how to build and deploy a containerized Go web server

Create application with commands:
	- `kubectl apply -f helloweb-app.yaml`
	- `kubectl apply -f helloweb-service.yaml`
	- `kubectl get svc service-helloweb -o yaml`

Deploy this application on [Amazon Elastic Kubernetes Service](https://aws.amazon.com/eks).

This repository also contains:

- `main.go` contains the HTTP server implementation. It responds to all HTTP
  requests with a  `Hello, world!` response.
- `Dockerfile` is used to build the Docker image for the application.



